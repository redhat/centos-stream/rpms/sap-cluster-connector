#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the sap_cluster_connector upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="sap_cluster_connector-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global sap_script_prefix sap_cluster_connector
%global sap_script_hash ba8c22e

Name:       sap-cluster-connector
Summary:    SAP cluster connector script
Version:    3.0.1
Release:    10%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}
License:    GPLv2+
URL:        https://github.com/redhat-sap/sap_cluster_connector
%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
Group:      System Environment/Base
%else
Group:      Productivity/Clustering/HA
%endif
Source0:    %{sap_script_prefix}-%{sap_script_hash}.tar.gz

BuildArch:  noarch

BuildRequires: perl-generators

Requires:   resource-agents-sap >= 4.8.0
Requires:   perl-interpreter

%description
The SAP connector script interface with Pacemaker to allow SAP
instances to be managed in a cluster environment.

%prep
%setup -q -n %{sap_script_prefix}-%{sap_script_hash}

%build

%install
rm -rf %{buildroot}
test -d %{buildroot}/%{_bindir} || mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_datadir}/sap_cluster_connector
mkdir -p %{buildroot}/%{_mandir}/man8
cp sap_cluster_connector %{buildroot}/%{_bindir}
cp -rv {run_checks,checks} %{buildroot}/%{_datadir}/sap_cluster_connector
gzip man/*.8
cp man/*.8.gz %{buildroot}/%{_mandir}/man8

%files
%defattr(-,root,root)
%{_bindir}/sap_cluster_connector
%{_mandir}/man8/sap_cluster_connector*
%{_datadir}/sap_cluster_connector

%changelog
* Thu Feb 20 2025 Janine Fuchs <jfuchs@redhat.com> - 3.0.1-10
- Fix output parser mismatch since pacemaker 2.1.6.

  Resolves: RHEL-80231

* Wed Jan 29 2025 Janine Fuchs <jfuchs@redhat.com> - 3.0.1-9
- Fix compatibility with pacemaker 3.0.

  Resolves: RHEL-76764

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 3.0.1-8.1
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Tue Jul 02 2024 Janine Fuchs <jfuchs@redhat.com> - 3.0.1-8
- Add package to RHEL 10.

  Resolves: RHEL-40576

- Changed perl dependency to perl-interpreter.

  Resolves: RHEL-45726

# vim:set ai ts=2 sw=2 sts=2 et:
